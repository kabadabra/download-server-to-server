<!DOCTYPE html>
<html>
<head>
	<title>Download File from Server</title>

	<!-- Latest compiled and minified CSS -->
	<link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css">

	<!-- Optional theme -->
	<link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap-theme.min.css">

	<!-- Latest compiled and minified JavaScript -->
	<script src="//maxcdn.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js"></script>

	<style type="text/css">
	body, html {
		margin: 20px;
	}
	</style>

</head>

<?php
error_reporting(E_ERROR);
set_time_limit(0);

if($_SERVER['REQUEST_METHOD'] == 'POST') {
	$file_to_download = trim($_POST['file_path']);
	$output_filename = trim($_POST['file_name']);
	$file_extract = trim($_POST['file_extract']);
	$file_delete = trim($_POST['file_delete']);

	//Error Checking
	if($file_to_download == '') {
		?>
		<div class="alert alert-danger" role="alert"><strong>Error:</strong> You have not entered a URL to download</div>
		<br /><br />
		<a href="javascript:;" onclick="history.go(-1);">Go Back</a>
		<?php

		die();
	}

	$file_headers = @get_headers($file_to_download);
	if($file_headers[0] == 'HTTP/1.1 404 Not Found') {
		?>
		<div class="alert alert-danger" role="alert"><strong>Error:</strong> File could not be found. Please make sure the URL is correct and that the archive exists.</div>
		<br /><br />
		<a href="javascript:;" onclick="history.go(-1);">Go Back</a>
		<?php

		die();
	}

	// Don't allow empty output filename
	if($output_filename == '') {
		$output_filename = 'latest.zip';
	}

	// File to save the contents to
	$fp = fopen ($output_filename, 'w+');

	$url = $file_to_download;

	// Here is the file we are downloading, replace spaces with %20
	$ch = curl_init(str_replace(" ","%20",$url));

	curl_setopt($ch, CURLOPT_TIMEOUT, 50000);

	// give curl the file pointer so that it can write to it
	curl_setopt($ch, CURLOPT_FILE, $fp);
	curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);

	$data = curl_exec($ch);//get curl response

	// done
	curl_close($ch);

	// Extract?
	if($file_extract == "yes") {
		unzip($output_filename, false, true, true);
	}

	// Delete downloaded archive?
	if($file_extract == "yes") {
		if(file_exists($output_filename)) {
			unlink($output_filename);
		}
	}
}

/**
* Unzip the source_file in the destination dir
*
* @param   string      The path to the ZIP-file.
* @param   string      The path where the zipfile should be unpacked, if false the directory of the zip-file is used
* @param   boolean     Indicates if the files will be unpacked in a directory with the name of the zip-file (true) or not (false) (only if the destination directory is set to false!)
* @param   boolean     Overwrite existing files (true) or not (false)
*
* @return  boolean     Succesful or not
*/
function unzip($src_file, $dest_dir=false, $create_zip_name_dir=true, $overwrite=true)
{
	if ($zip = zip_open($src_file))
	{
		if ($zip)
		{
			$splitter = ($create_zip_name_dir === true) ? "." : "/";
			//if ($dest_dir === false) $dest_dir = substr($src_file, 0, strrpos($src_file, $splitter))."/";
			$dest_dir = '/';
      		// Create the directories to the destination dir if they don't already exist
			create_dirs($dest_dir);

      		// For every file in the zip-packet
			while ($zip_entry = zip_read($zip))
			{
        		// Now we're going to create the directories in the destination directories

        		// If the file is not in the root dir
				$pos_last_slash = strrpos(zip_entry_name($zip_entry), "/");
				if ($pos_last_slash !== false)
				{
          			// Create the directory where the zip-entry should be saved (with a "/" at the end)
					create_dirs($dest_dir.substr(zip_entry_name($zip_entry), 0, $pos_last_slash+1));
				}

        		// Open the entry
				if (zip_entry_open($zip,$zip_entry,"r"))
				{

          			// The name of the file to save on the disk
					$file_name = $dest_dir.zip_entry_name($zip_entry);

          			// Check if the files should be overwritten or not
					if ($overwrite === true || $overwrite === false && !is_file($file_name))
					{
            			// Get the content of the zip entry
						$fstream = zip_entry_read($zip_entry, zip_entry_filesize($zip_entry));

						file_put_contents($file_name, $fstream );
            			// Set the rights
						chmod($file_name, 0777);
						echo "save: ".$file_name."<br />";
					}

          			// Close the entry
					zip_entry_close($zip_entry);
				}
			}
      		// Close the zip-file
			zip_close($zip);
		}
	}
	else
	{
		return false;
	}

	return true;
}

/**
* This function creates recursive directories if it doesn't already exist
*
* @param String  The path that should be created
*
* @return  void
*/
function create_dirs($path)
{
	if (!is_dir($path))
	{
		$directory_path = "";
		$directories = explode("/",$path);
		array_pop($directories);

		foreach($directories as $directory)
		{
			$directory_path .= $directory."/";
			if (!is_dir($directory_path))
			{
				mkdir($directory_path);
				chmod($directory_path, 0777);
			}
		}
	}
}
?>

<body>

	<div class="page-header">
	<h1>Download Server to Server</h1>
	</div>

	<div class="row">
		<div class="col-md-12">
			<form role="form" action="<?php $_SERVER['PHP_SELF']; ?>" method="post">
				<div class="form-group">
				<label for="exampleInputEmail1">Archive to download</label>
					<input type="text" name="file_path" class="form-control" id="exampleInputEmail1" placeholder="http://www.example.com/backup.zip">
				</div>
				<div class="form-group">
					<label for="exampleInputPassword1">New filename?</label>
					<input type="text" name="file_name" class="form-control" id="exampleInputPassword1" placeholder="latest.zip">
				</div>
				<div class="checkbox">
					<label>
						<input type="checkbox" name="file_extract" value="yes"> Extract archive after download?
					</label>
				</div>
				<div class="checkbox">
					<label>
						<input type="checkbox" name="file_delete" value="yes"> Delete archive after extract / download?
					</label>
				</div>
				<br />
				<button type="submit" class="btn btn-default" value="submit">Submit</button>
			</form>
			<br />
			<br />
			<div class="alert alert-warning" role="alert"><strong>Note:</strong> Please delete "download.php" after use as this would be a huge security risk to keep this on the server</div>
		</div>
	</div>
</body>
</html>